package com.geccocrawler.gecco.demo;

import java.util.List;

import org.omg.PortableServer.POAManagerPackage.State;

import com.geccocrawler.gecco.GeccoEngine;
import com.geccocrawler.gecco.annotation.Gecco;
import com.geccocrawler.gecco.annotation.Href;
import com.geccocrawler.gecco.annotation.HtmlField;
import com.geccocrawler.gecco.annotation.Request;
import com.geccocrawler.gecco.annotation.RequestParameter;
import com.geccocrawler.gecco.annotation.Text;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.spider.HtmlBean;

@Gecco(matchUrl = "http://www.aastocks.com/sc/ipo/sponsor.aspx?&hidTab1Sort=&hidTab1Order=&hidTab2Sort=&hidTab2Order=&hidTab2F1=&hidTab2F2=&hidPage={num}", pipelines = "consolePipeline", timeout = 1000)
public class HKNewStock implements HtmlBean {

	private static final long serialVersionUID = -7127412585200687225L;

	@Request
	private HttpRequest request;

	@RequestParameter("num")
	private String num;

	@Text(own = false)
	@HtmlField(cssPath = "div#rpt2 > table > tbody > tr.ADR,div#rpt2 > table > tbody > tr.DR")
	private List<String> info;

	public HttpRequest getRequest() {
		return request;
	}

	public void setRequest(HttpRequest request) {
		this.request = request;
	}

	public List<String> getinfo() {
		return info;
	}

	public void setinfo(List<String> readme) {
		this.info = readme;
	}

	public static void main(String[] args) {
		String url2 = "http://www.aastocks.com/sc/ipo/sponsor.aspx?hidPage=";
		GeccoEngine engine = GeccoEngine.create().classpath("com.geccocrawler.gecco.demo");

		for (int i = 1; i < 30; i++) {
			String turl = url2 + i;
			System.out.println(turl);
			// 开始抓取的页面地址,这种方式可以进行遍历
			try {
				if (i == 1) {
					engine.start(turl).interval(3000).start();
				} else {
					engine.start(turl);
				}

			} catch (Throwable e) {
				System.out.println(e.getMessage());
				e.printStackTrace(System.out);
			}
		}

	}

}
